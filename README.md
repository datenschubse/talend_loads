# Talend Load's

This is a project, that keep some of my ideas how to use the Talend Open Studio for Data Integration in different scenarios.

## Getting Started

This section try's to explain what kind of steps you have to do to get the examples up and running.

### Preamble

Goal of this project is to give some insight into the Talend Open Studio for Data Integration. With some SQL code snippets to create a start schema.   
It is **not** _production_ ready - in every case: setup, passwords, firewall settings, logging, monitoring, edge cases, schema comments etc.!   
It is not a complete developement project - where you can clone, source and run (no docker, schema handling etc. implementation).   
If you want to share ideas how to do that or work on that - just ask, just do it. Every help, brainstorming etc. is welcome!   


### Prerequisites

* a PostgreSQL database, a good intallation guide for ubuntu/debian is here [DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-16-04)
* for the optional schema versioning you can started with [sqitch](http://sqitch.org/), on [github](https://github.com/theory/sqitch) & a tutorial you find via [David E. Wheeler](https://metacpan.org/pod/sqitchtutorial), just use the SQL files in every subdirectory...
* use this link for the [Talend Open Studio for Data Integration](https://www.talend.com/download/talend-open-studio/#dataintegration), click on "DATA INTEGRATION" and download...

### Other help

* every talend load comes arround with multiple called "context" groups
* at least you need a local PostgresQL database installed and two different databases created `play_dev` & `play_test`, login with you user & dont forget to adapt the context variables to your credentials in the talend loads:

```sql
CREATE DATABASE play_dev;
CREATE DATABASE play_test;
```
* how to get a talend load running please have a look into [Talend by Example](https://www.talendbyexample.com/talend-job-deployment-reference.html)
* at least you have to do a `Build Job` in you Taled eclipse studio, export it somewhere to you (local) system and do:

```bash
unzip start_control_currency_load_0.1.zip 
cd start_control_currency_load/
chmod +x start_control_currency_load_run.sh
./start_control_currency_load_run.sh --context=test
```

## currency

This is a idea where:   

> _Hey I have price relevant data from different countries with different currencies where I run my **target** operations. I need reports for every country in it's own currency for the country manager, but want to have an overview of all revenues for my headquarter that runs in a **base** currency!_

Obviously you can exhange the base and target definition, just adapt it and dont forget to change the calculation (see later)...

After running the DDL, you find 3 different tables with the following structure there (short version):

```sql
play_test=# \dt play.
            List of relations
 Schema |      Name      | Type  | Owner 
--------+----------------+-------+-------
 play   | currency_dim   | table | rico
 play   | currency_facts | table | rico
 play   | date_dim       | table | rico
(3 rows)

play_test=# \d play.
                                             Table "play.currency_dim"
      Column      |         Type         |                                Modifiers                                
------------------+----------------------+-------------------------------------------------------------------------
 currency_sk      | integer              | not null default nextval('play.currency_dim_currency_sk_seq'::regclass)
 currency_code    | character varying(3) | 
 is_base_currency | boolean              | default false
Indexes:
    "currency_dim_pkey" PRIMARY KEY, btree (currency_sk)
    "currency_dim_currency_code_key" UNIQUE CONSTRAINT, btree (currency_code)
Check constraints:
    "currency_dim_currency_code_check" CHECK (length(currency_code::text) = 3)
Referenced by:
    TABLE "play.currency_facts" CONSTRAINT "currency_facts_currency_base_sk_fkey" FOREIGN KEY (currency_base_sk) REFERENCES play.currency_dim(currency_sk)
    TABLE "play.currency_facts" CONSTRAINT "currency_facts_currency_target_sk_fkey" FOREIGN KEY (currency_target_sk) REFERENCES play.currency_dim(currency_sk)

          Table "play.currency_facts"
       Column       |     Type      | Modifiers 
--------------------+---------------+-----------
 date_sk            | integer       | not null
 currency_base_sk   | integer       | not null
 currency_target_sk | integer       | not null
 change             | numeric(14,4) | 
Indexes:
    "currency_facts_pkey" PRIMARY KEY, btree (date_sk, currency_base_sk, currency_target_sk)
Foreign-key constraints:
    "currency_facts_currency_base_sk_fkey" FOREIGN KEY (currency_base_sk) REFERENCES play.currency_dim(currency_sk)
    "currency_facts_currency_target_sk_fkey" FOREIGN KEY (currency_target_sk) REFERENCES play.currency_dim(currency_sk)
    "currency_facts_date_sk_fkey" FOREIGN KEY (date_sk) REFERENCES play.date_dim(date_sk)

                                     Table "play.date_dim"
      Column      |   Type   |                            Modifiers                            
------------------+----------+-----------------------------------------------------------------
 date_sk          | integer  | not null default nextval('play.date_dim_date_sk_seq'::regclass)
 date_date        | date     | 
 date_day         | integer  | 
 date_month       | integer  | 
 date_year        | smallint | 
 day_of_year      | smallint | 
 iso_day_of_week  | smallint | 
 iso_week_of_year | smallint | 
Indexes:
    "date_dim_pkey" PRIMARY KEY, btree (date_sk)
    "date_dimension_nk" UNIQUE CONSTRAINT, btree (date_date)
Referenced by:
    TABLE "play.currency_facts" CONSTRAINT "currency_facts_date_sk_fkey" FOREIGN KEY (date_sk) REFERENCES play.date_dim(date_sk)

```

**After tunning the load**, just try this **_hey for the first 3 days of January 2010 I have the following revenues and want to see the prices in EUR_**:

```sql
play_test=# WITH 
  create_data AS (
  SELECT '2010-01-01'::DATE AS date_date, 'EUR'::VARCHAR(3) AS target_currency, 11.11::NUMERIC(14,4) AS a_price UNION
  SELECT '2010-02-01'::DATE AS date_date, 'GBP'::VARCHAR(3) AS target_currency, 12.12::NUMERIC(14,4) AS a_price UNION
  SELECT '2010-03-01'::DATE AS date_date, 'RUB'::VARCHAR(3) AS target_currency, 333.33::NUMERIC(14,4) AS a_price
)
SELECT 
  date_dim.date_date, 
  base.currency_code AS base_currency, 
  a_price / change AS a_converted_price,
  change,
  a_price,
  target.currency_code AS a_target_currency_code
FROM 
  create_data JOIN
  play.date_dim ON date_dim.date_date = create_data.date_date JOIN
  play.currency_facts USING (date_sk) JOIN
  play.currency_dim AS base ON currency_base_sk = base.currency_sk JOIN
  play.currency_dim AS target ON currency_target_sk = target.currency_sk AND create_data.target_currency = target.currency_code
WHERE 
  base.currency_code = 'EUR'
ORDER BY 1;
 date_date  | base_currency |  a_converted_price  | change  | a_price  | a_target_currency_code 
------------+---------------+---------------------+---------+----------+------------------------
 2010-01-01 | EUR           | 11.1100000000000000 |  1.0000 |  11.1100 | EUR
 2010-02-01 | EUR           | 13.8530117727740313 |  0.8749 |  12.1200 | GBP
 2010-03-01 | EUR           |  8.2208301477297951 | 40.5470 | 333.3300 | RUB
```

...any questions? Now you can summarize it, present it or whatever you have to do...

**Hint**
* as a first start EUR is defined as a base currency, after the first loads the `currency_dim` table is prefilled with available currencies. Just move another currency as a second base while changing the `is_base_currency`, eg. on USD, flag to true via `UPDATE...` and run the load again...  

## Thanks to...

* a really good overview of APIs for _some data playground steps_ are created via [Todd Motto](https://github.com/toddmotto/public-apis)
* for [fixer.io](http://fixer.io/) at [Hakan Ensari](https://github.com/hakanensari)    

> Of course your implementation has a rate limit, why someone should do more than 4 request per second? Sorry... :speak_no_evil:
