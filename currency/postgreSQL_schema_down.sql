-- Revert play:createCurrencyDimension from pg

BEGIN;

  DROP TABLE IF EXISTS play.currency_dim;

COMMIT;
-- Revert play:createCurrencyFacts from pg

BEGIN;

  DROP TABLE IF EXISTS play.currency_facts;

COMMIT;
-- Revert play:createDateDimension from pg

BEGIN;

DROP TABLE IF EXISTS play.date_dim;

COMMIT;
-- Revert play:createSchema from pg

BEGIN;

DROP SCHEMA IF EXISTS play;

COMMIT;
