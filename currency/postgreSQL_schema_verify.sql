-- Verify play:createCurrencyDimension on pg

BEGIN;

  SELECT 
    1 / COUNT(*) 
  FROM information_schema.tables 
  WHERE 
    table_name = 'currency_dim' AND 
    table_schema = 'play';

ROLLBACK;
-- Verify play:createCurrencyFacts on pg

BEGIN;

  SELECT 
    1 / COUNT(*) 
  FROM information_schema.tables 
  WHERE 
    table_name = 'currency_facts' AND 
    table_schema = 'play';

ROLLBACK;
-- Verify play:createDateDimension on pg

BEGIN;

  SELECT 1/COUNT(*) FROM play.date_dim;

ROLLBACK;
-- Verify play:createSchema on pg

BEGIN;

  SELECT pg_catalog.has_schema_privilege('play', 'usage');

  SELECT 1/COUNT(*) FROM information_schema.schemata WHERE schema_name = 'play';

ROLLBACK;
