-- Deploy play:createCurrencyDimension to pg

BEGIN;

  DROP TABLE IF EXISTS play.currency_dim;

  CREATE TABLE play.currency_dim (
    currency_sk SERIAL PRIMARY KEY,
    currency_code VARCHAR(3) UNIQUE CHECK(LENGTH(currency_code) = 3),
    is_base_currency BOOLEAN DEFAULT false
  );

COMMIT;
-- Deploy play:createCurrencyFacts to pg

BEGIN;

  DROP TABLE IF EXISTS play.currency_facts;

  CREATE TABLE play.currency_facts (
    date_sk INTEGER  REFERENCES play.date_dim (date_sk),
    currency_base_sk INTEGER  REFERENCES play.currency_dim (currency_sk),
    currency_target_sk INTEGER  REFERENCES play.currency_dim (currency_sk),
    change NUMERIC(14,4),
    CONSTRAINT currency_facts_pkey PRIMARY KEY (date_sk, currency_base_sk, currency_target_sk)
  );


COMMIT;
-- Deploy play:createDateDimension to pg

BEGIN;

DROP TABLE IF EXISTS play.date_dim;

CREATE TABLE play.date_dim (
  date_sk serial PRIMARY KEY,
  date_date date CONSTRAINT date_dimension_nk UNIQUE,
  date_day integer,
  date_month integer,
  date_year smallint,
  day_of_year smallint,
  iso_day_of_week smallint,
  iso_week_of_year smallint
);

WITH
  create_date_series AS (
    SELECT * from generate_series('2010-01-01'::date, '2050-12-31'::date, '1 day') as date_date),
  cast_to_date AS (
    SELECT date_date::date as date_date FROM create_date_series),
  extract_date_dim AS (
    SELECT 
      date_date as date_date,
      to_char(date_date, 'YYYYMMDD')::int AS date_day,
      to_char(date_date, 'YYYYMM')::int AS date_month,
      to_char(date_date, 'YYYY')::int AS date_year,
      EXTRACT(doy FROM date_date)::int AS day_of_year,
      EXTRACT(isodow FROM date_date) iso_day_of_week,
      EXTRACT(week FROM date_date)::int AS iso_week_of_year
    FROM cast_to_date)
INSERT INTO play.date_dim (date_date, date_day, date_month, date_year, day_of_year, iso_day_of_week, iso_week_of_year) 
  SELECT date_date, date_day, date_month, date_year, day_of_year, iso_day_of_week, iso_week_of_year FROM extract_date_dim;

COMMIT;
-- Deploy play:createSchema to pg

BEGIN;

  CREATE SCHEMA play;

COMMIT;
